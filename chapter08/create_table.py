import pymysql

# 连接数据库
conn = pymysql.connect(
    host='127.0.0.1',
    port=3306,
    user='root',
    passwd='123456',
    db='feizhi',
    charset='utf8'
)

#　创建游标
cursor = conn.cursor()

# 定义创建部门表的SQL语句
create_dep = '''CREATE TABLE departments(
dep_id INT, dep_name VARCHAR(50),
PRIMARY KEY(dep_id)
)'''
# 定义创建员工表的语句，与部门表具有外键约束
create_emp = '''CREATE TABLE employees(
emp_id INT, emp_name VARCHAR(50), email VARCHAR(50), dep_id INT,
PRIMARY KEY(emp_id), FOREIGN KEY(dep_id) REFERENCES departments(dep_id)
)'''
# 定义创建工资表的语句，与员工表有外键约束
create_sal = '''CREATE TABLE salary(
id INT, date DATE, emp_id INT, basic INT, awards INT,
PRIMARY KEY(id), FOREIGN KEY(emp_id) REFERENCES employees(emp_id)
)'''

# 通过游标执行SQL语句
cursor.execute(create_dep)
cursor.execute(create_emp)
cursor.execute(create_sal)

# 确认更改
conn.commit()
# 关闭游标、关闭到数据库的连接
cursor.close()
conn.close()
