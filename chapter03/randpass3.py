import random
import string

all_chs = string.ascii_letters + string.digits
result = [random.choice(all_chs) for n in range(8)]

print(''.join(result))
