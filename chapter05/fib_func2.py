def gen_fib():
    fib = [0, 1]

    length = int(input("数列长度: "))
    for i in range(length - 2):
        fib.append(fib[-1] + fib[-2])

    return fib

result = gen_fib()
new_nums = [i * 2 for i in result]
print(new_nums)
