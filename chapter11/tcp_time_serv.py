import socket
import threading
from time import strftime

class TcpTimeServ:
    def __init__(self, host, port):
        self.addr = (host, port)
        self.serv = socket.socket()
        self.serv.bind(self.addr)
        self.serv.setsockopt(
            socket.SOL_SOCKET, socket.SO_REUSEADDR, 1
        )
        self.serv.listen(1)

    def chat(self, cli_sock):
        while True:
            data = cli_sock.recv(1024)
            if data.strip() == b'quit':
                break
            data = '[%s] %s' % (strftime('%H:%M:%S'), data.decode())
            cli_sock.send(data.encode())

        cli_sock.close()

    def mainloop(self):
        while True:
            try:
                cli_sock, cli_addr = self.serv.accept()
            except KeyboardInterrupt:
                break
            t = threading.Thread(target=self.chat, args=(cli_sock,))
            t.start()

        self.serv.close()

if __name__ == '__main__':
    host = ''
    port = 12345
    tcpserv = TcpTimeServ(host, port)
    tcpserv.mainloop()
